import java.util.Scanner;

public class Ejercicio {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int num1;
		int num2;
		
		System.out.print("Escriba el primer entero: ");
		num1 = sc.nextInt();
	
		System.out.print("Escriba el segundo entero: ");
		num2 = sc.nextInt();
		
		if (num1 % 2 == 0 && num2 % 2 ==0) {
			int num3 = num1*num2;
			System.out.print("Como los dos numeros son pares, entonces su multiplicación es: " + num3);	
		}
		
		if (num1 % 2 >= 1 && num2 % 2 >=1) {
			int num4 = num1/num2;
			System.out.print("Como los dos numeros son impares, entonces su división es: " + num4);	
		}
			
		if (num1 % 2 == 0 && num2 % 2 >=1 ) {
			System.out.print("Como los numeros son par o impar, no se puede realizar una operación");
		}
		
		if (num1 % 2 >= 1 && num2 % 2 ==0 ) {
			System.out.print("Como los numeros son par o impar, no se puede realizar una operación");
		}
		sc.close();
	}
}