import java.util.Scanner;

public class EjercicioDescuento { 

	private static Scanner sc;

	public static void main(String[] args) {
	//En una tienda se aplica un 20% de descuento a ni�os, 10% para adultos
	//Tomando en cuenta que se considera ni�o a aquellos que tienen una edad comprendida entre 0 - 17 a�os.
	
		sc = new Scanner(System.in);		
		int num1;
		
		double num2;
			
		System.out.println("Inserte la edad de la persona: ");
		num1 = sc.nextInt();
		
		System.out.println("Ingrese precio de la compra: ");
		num2 = sc.nextInt();
	
	if (num1 > 17) { 
		num2 = num2*0.8;
		System.out.println("El precio total es de: $"+num2);
		
	}
	
	if (num1 <= 17) {
		num2 = num2*0.9;
		System.out.println("El precio total es de: $"+num2);
	}
	
	}

}